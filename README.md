# Docker Compose Setup

This is an example Docker Compose setup that includes two Debian images, each connected to a separate Docker network. This setup can be used to simulate containers with different network access permissions.

## Prerequisites

- Docker Engine and Docker Compose installed on your system
- Basic knowledge of Docker Compose and Docker networks

## Usage

1. Clone this repository to your local machine:

   \`\`\`bash
   git clone https://gitlab.com/hung135/air_gapped_dev.git
   cd air_gapped_dev
   \`\`\`

2. Start the Docker Compose stack:

   \`\`\`bash
   docker-compose up -d
   \`\`\`

3. Check the status of the Docker Compose stack:

   \`\`\`bash
   docker-compose ps
   \`\`\`

   You should see two running containers: \`no_internet\` and \`with_internet\`.

4. Check the network connectivity of the \`with_internet\` container:

   \`\`\`python
   docker-compose exec with_internet ping www.google.com
   \`\`\`

   You should see output similar to the following:

   \`\`\`python
   PING www.google.com (172.217.10.36) 56(84) bytes of data.
   64 bytes from lga34s17-in-f4.1e100.net (172.217.10.36): icmp_seq=1 ttl=113 time=9.08 ms
   ...
   \`\`\`

5. Check the network connectivity of the \`no_internet\` container:

   \`\`\`python
   docker-compose exec no_internet ping www.google.com
   \`\`\`

   You should see output similar to the following:

   \`\`\`makefile
   ping: www.google.com: Name or service not known
   \`\`\`

   This indicates that the \`no_internet\` container is unable to access the internet.

## Docker Compose Configuration

The \`docker-compose.yml\` file defines the Docker Compose services and networks used in this setup.

### Services

This setup includes two services:

- \`no_internet\`: This service runs a \`debian:latest\` image and has no internet access.
- \`with_internet\`: This service runs a \`debian:latest\` image and has internet access.

### Networks

This setup includes one custom Docker network for each service:

- \`isolated_network_no_internet\`: This network is used by the \`no_internet\` service and is isolated from external networks by default.
- \`isolated_network_with_internet\`: This network is used by the \`with_internet\` service and has access to the host's default network.

### Environment Variables

The \`no_internet\` service is configured with environment variables \`http_proxy\` and \`https_proxy\` that point to a proxy server that is not available, effectively blocking all outgoing network traffic.

### Kernel Capabilities

The \`no_internet\` service is configured to drop certain Linux kernel capabilities that could be used to bypass network restrictions (\`NET_RAW\` and \`NET_ADMIN\`).

## Customization

If you need to customize this setup, you can modify the following files:

- \`docker-compose.yml\`: This file defines the Docker Compose services and networks used in this setup.
- \`Dockerfile.no_internet\`: This file defines the custom Debian image used by the \`no_internet\` service. You can modify this file to include additional software packages or configurations.
